var Knex = require('knex');
if (!global.Promise) {
	Promise = require('bluebird');
}
var NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = function(grunt) {
	grunt
			.initConfig({
				pkg : 'package.json',
				less : {
					development : {
						options : {
							sourceMap : true
						},
						files : {
							'WebContent/resource/style.css' : 'WebContent/resource/style.less',
						}
					},
					production : {
						options : {
							compress : true
						},
						files : {
							'WebContent/resource/style.css' : 'WebContent/resource/style.less',
						}
					}
				},
			});

	grunt.loadNpmTasks('grunt-contrib-less');

	grunt.registerTask('db:testinit', function() {
		dbLoadData(this.async());
	});

	grunt.registerTask('db:clean', function() {
		dbCleanData(this.async());
	});

	grunt.registerTask('build', [ 'less:' + NODE_ENV ]);
};

function dbLoadData(done) {
	var db = initDb();

	function loadData(tableName, columnNames) {
		var sql;

		sql = 'load data local infile \'sql/'
				+ tableName
				+ '.csv\' into table '
				+ tableName
				+ ' fields terminated by \',\' optionally enclosed by \'"\' ignore 1 lines ('
				+ columnNames + ')';
		console.log(sql);
		return db.raw(sql);
	}

	Promise
			.all(
					[
							[ 'ink_cartridge',
									'id, name, lower_limit_number, number, deleted_at' ],
							[ 'user', 'id, name, password, deleted_at' ],
							[ 'replace_log',
									'id, time, user_id, ink_cartridge_id, ink_cartridge_number' ],
							[
									'take_out_log',
									'id, time, user_id, student_id, student_name, ink_cartridge_id, ink_cartridge_number' ] ]
							.map(function(table) {
								return loadData(table[0], table[1]);
							})).then(done);
}

function dbCleanData(done) {
	var db = initDb();

	function cleanData(tableName) {
		var promises = [], sql1, sql2;

		sql1 = 'delete from ' + tableName;
		sql2 = 'alter table ' + tableName + ' auto_increment = 1';
		console.log(sql1);
		promises.push(db.raw(sql1));
		console.log(sql2);
		promises.push(db.raw(sql2));
		return Promise.all(promises);
	}

	Promise.all(
			[ 'take_out_log', 'replace_log', 'user', 'ink_cartridge' ]
					.map(function(tableName) {
						return cleanData(tableName);
					})).then(done);
}

function initDb() {
	return Knex.initialize({
		client : 'mysql',
		connection : {
			host : '127.0.0.1',
			user : 'root',
			password : '',
			database : 'inkstockmanager',
			charset : 'utf8'
		}
	});
}