(function(global) {
	'use strict';

	/**
	 * http://c4se.hatenablog.com/entry/2014/03/25/012252
	 * 
	 * @param {function(Object...):Object}
	 *            _super
	 * @param {function(Object...):Object}
	 *            constructor
	 * @return {function(Object...):Object}
	 */
	function extend(_super, constructor) {
		var _class = null, toString;

		_class = function() {
			if (!(this instanceof _class)) {
				return new (Function.prototype.bind.apply(_class, [ null ]
						.concat(Array.from(arguments))));
			}
			_super.apply(this, arguments);
			return constructor.apply(this, arguments);
		};
		_class.prototype = Object.create(_super.prototype);
		_class.prototype.constructor = _class;
		toString = Function.prototype.toString;
		Function.prototype.toString = function() {
			if (this === _class) {
				return constructor.toString();
			}
			return toString.call(this);
		};
		return _class;
	}

	/**
	 * http://c4se.hatenablog.com/entry/2014/03/29/074201
	 * 
	 * @param (function(Object...):Object}
	 *            fun
	 * @param {Object}
	 *            _this
	 * @param {Object.
	 *            <string,Object>} args
	 * @return {function(Object...):Object}
	 */
	function inject(fun, _this, args) {
		var params;

		params = fun.toString().replace(/\r?\n/g, ' ').match(
				/function[^\(]*\(([^\)]*)\)/)[1].split(',').map(
				function(param) {
					return param.trim();
				});
		return function() {
			var i = 0, iz = 0, j = 0, _arguments = [];

			j = 0;
			for (i = 0, iz = params.length; i < iz; ++i) {
				if (args[params[i]] !== void 0) {
					_arguments[i] = args[params[i]];
				} else {
					_arguments[i] = arguments[j];
					++j;
				}
			}
			fun.apply(_this, _arguments);
		};
	}

	global.Functil = {
		extend : extend,
		inject : inject,
		include : function() {
			Function.prototype.extend = function(constructor) {
				return extend(this, constructor);
			};
			Function.prototype.inject = function(_this, args) {
				return inject(this, _this, args);
			};
		}
	};
}(this.self || global));