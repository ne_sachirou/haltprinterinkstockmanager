(function() {
	'use strict';

	// ES5
	if (!Array.isArray) {
		/**
		 * @param {Object}
		 *            obj
		 * @returns {Boolean}
		 */
		Array.isArray = function(obj) {
			return Object.prototype.toString.call(obj) === '[object Array]';
		};
	}

	// ES6
	if (!Array.from) {
		/**
		 * @param {Object}
		 *            list
		 * @returns {Array}
		 */
		Array.from = function(list) {
			return [].slice.call(list);
		};
	}

}());