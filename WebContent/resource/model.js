(function(global) {
	'use strict';

	/**
	 * @constructor
	 */
	function Model() {
	}

	/**
	 * @param {string}
	 *            path
	 * @param {Object.
	 *            <string,Object>=} data
	 * @param {String=}
	 *            method
	 * @return {Promise}
	 */
	Model.getJson = function(path, data, method) {
		var request = new XMLHttpRequest(),
		/** @type {string} */
		form;

		if (!method) {
			method = 'GET';
		}
		method = method.toUpperCase();
		form = (function(data) {
			var form = [], i = 0, iz = 0, keys, key, value;

			if (!data) {
				return;
			}
			keys = Object.keys(data);
			for (i = 0, iz = keys.length; i < iz; ++i) {
				key = keys[i];
				value = data[key];
				if (Array.isArray(value)) {
					value.forEach(function(value) {
						form.push(encodeURIComponent(key) + '='
								+ encodeURIComponent(value));
					});
				} else {
					form.push(encodeURIComponent(key) + '='
							+ encodeURIComponent(value));
				}
			}
			return form.join('&');
		}(data));
		if (method !== 'GET') {
			request.open(method, path, true);
			request.setRequestHeader('Content-type',
					'application/x-www-form-urlencoded');
			request.send(form);
		} else {
			request.open('GET', path + (path.indexOf('?') >= 0 ? '&' : '?')
					+ form, true);
			request.send();
		}
		return new Promise(function(resolve, reject) {
			request.onreadystatechange = function() {
				var response;

				if (request.readyState === 4) {
					if (request.status !== 200) {
						try {
							return reject(new Error(JSON
									.parse(request.responseText).error));
						} catch (_) {
							return reject(new Error(request.responseText));
						}
					}
					try {
						response = JSON.parse(request.responseText);
					} catch (err) {
						return reject(err);
					}
					resolve(response);
				}
			};
		});
	};

	/**
	 * @param {Object.
	 *            <string,Object>} hash
	 * @return {Model}
	 */
	Model.prototype.fromHash = function(hash) {
		var i = 0, iz = 0, isNull = true, keys, key;

		keys = Object.keys(hash);
		for (i = 0, iz = keys.length; i < iz; ++i) {
			key = keys[i];
			if (this[key] !== void 0) {
				isNull = false;
				this[key] = hash[key];
			}
		}
		return isNull ? null : this;
	};

	/**
	 * 
	 * @param {string}
	 *            path
	 * @param {?Object.
	 *            <string,Object>} data
	 * @param {string=}
	 *            method GET|POST|PUT|DELETE
	 * @param {?string}
	 *            key
	 * @returns {Promise}
	 */
	Model.prototype.getResource = function(path, data, method, key) {
		var me = this;

		return new Promise(function(resolve, reject) {
			Model.getJson(path, data, method).then(function(response) {
				var tmp;

				if (!response) {
					response = null;
				} else if (Array.isArray(response)) {
					response = response.map(function(elm) {
						return new (me.constructor)().fromHash(elm);
					});
				} else {
					response = new (me.constructor)().fromHash(response);
				}
				if (key) {
					tmp = {};
					tmp[key] = response;
					response = tmp;
				}
				resolve(response);
			}, function(err) {
				reject(err);
			});
		});
	};

	global.Model = Model;
}(this.self || global));