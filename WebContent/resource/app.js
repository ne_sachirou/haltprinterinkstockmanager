(function(global) {
	'use strict';

	Functil.include();

	/**
	 * @constructor
	 */
	var InkCartridge = Model.extend(function() {
		this.id = 0;
		this.name = '';
		this.lowerLimitNumber = 0;
		this.number = 0;

		this.replaceNumber = 0;
	});

	Object.defineProperty(InkCartridge.prototype, 'color', {
		/**
		 * @return {string} 'cyan'|'magenta'|'yellow'|'black'
		 */
		get : function() {
			var name = this.name;

			if (name.indexOf('青') >= 0 || name.indexOf('シアン') >= 0
					|| /\WC(?:\W|$)/i.test(name)) {
				return 'cyan';
			}
			if (name.indexOf('マゼンタ') >= 0 || /\WM(?:\W|$)/i.test(name)) {
				return 'magenta';
			}
			if (name.indexOf('黄') >= 0 || name.indexOf('イエロー') >= 0
					|| /\WY(?:\W|$)/i.test(name)) {
				return 'yellow';
			}
			if (name.indexOf('黒') >= 0 || name.indexOf('ブラック') >= 0
					|| /\W(?:BK|B|K)(?:\W|$)/i.test(name)) {
				return 'black';
			}
			return '';
		}
	});

	/**
	 * @constructor
	 */
	var User = Model.extend(function() {
		this.id = 0;
		this.name = '';
		/** @type {string} Raw password string which the user inputs. */
		this.password = '';
	});

	/**
	 * @constructor
	 */
	var TakeOutLog = Model.extend(function() {
	});

	/**
	 * @constructor
	 */
	var ReplaceLog = Model.extend(function() {
	});

	/**
	 * Singleton.
	 * 
	 * @constructor
	 */
	function App() {
		var me = this;

		if (!(this instanceof App)) {
			return new App();
		}
		App = function() {
			return me;
		};
		this._currentUser = null;
		this._routes = {};
		this.baseUrl = document.getElementsByTagName('base')[0]
				.getAttribute('href');
	}

	/**
	 * 
	 * @returns {Promise}
	 */
	App.prototype.init = function() {
		var me = this;

		$(document).on('click', 'a', function(evt) {
			var path = evt.target.getAttribute('href');

			if (URI(path).absoluteTo('/').host() !== location.hostname) {
				return;
			}
			evt.preventDefault();
			me.loadPage(path);
		});
		return this
				.currentUser()
				.then(
						function(currentUser) {
							if (!currentUser) {
								return new Promise(
										function(resolve) {
											document.getElementById('main').innerHTML = '<login-form />';
											resolve(me);
										});
							}
							return me.loadPage();
						});
	};

	/**
	 * 
	 * @param path
	 * @returns {Promise}
	 */
	App.prototype.loadPage = function(path) {
		var me = this, controller;

		if (path) {
			history.pushState(JSON.stringify({
				title : document.title
			}), '', path);
		}
		if (!path) {
			path = '/' + location.pathname.substring(this.baseUrl.length);
		}
		controller = this._matchRoute(path);
		return new Promise(function(resolve, reject) {
			controller.call();
			resolve(me);
		});
	};
	/*
	 * window.onpopstate = function(evt) { var state;
	 * 
	 * evt.preventDefault(); state = JSON.parse(evt.state); new
	 * App().loadPage(null, state); };
	 */

	/**
	 * 
	 * @param {string}
	 *            path
	 * @param {function(...)}
	 *            controller
	 * @returns {App}
	 */
	App.prototype.route = function(path, controller) {
		this._routes[path] = controller;
		return this;
	};

	/**
	 * 
	 * @param {?String}
	 *            path
	 * @returns {function()}
	 */
	App.prototype._matchRoute = function(path) {
		var i = 0, iz = 0, keys, key;

		function match(key, path) {
			var i = 0, iz = 0, params = {}, regex, matches, paramMatches;

			matches = key.match(/:\w+/g);
			if (!matches) {
				return false;
			}
			regex = new RegExp('^' + key.replace(/[\/]/, function(_1) {
				return '\\' + _1;
			}).replace(/:\w+/, '([^/]+)') + '$');
			paramMatches = path.match(regex);
			if (!paramMatches) {
				return false;
			}
			for (i = 0, iz = matches.length; i < iz; ++i) {
				params[matches[i].substring(1)] = paramMatches[i + 1];
			}
			return [ key, params ];
		}

		function fillGetParams(params) {
			params = location.search.substring(1).split('&').reduce(
					function(params, param) {
						if (!param) {
							return params;
						}
						param = param.split('=');
						params[param[0]] = param[1];
					}, params);
			params.params = Object.keys(params).reduce(function(accm, key) {
				accm[key] = accm[key] || params[key];
				return accm;
			}, {});
			return params;
		}

		if (this._routes[path]) {
			return this._routes[path].bind(this);
		}
		keys = Object.keys(this._routes);
		for (i = 0, iz = keys.length; i < iz; ++i) {
			key = match(keys[i], path);
			if (key) {
				return this._routes[key[0]].inject(this, fillGetParams(key[1]));
			}
		}
		return this._routes['default'] || function() {
			throw new Error('Implements default controller.');
		};
	};

	/**
	 * 
	 * @param user
	 * @returns {Promise}
	 */
	App.prototype.login = function(user) {
		var me = this;

		return new User().getResource('api/login', user, 'POST').then(
				function(currentUser) {
					me._currentUser = currentUser;
					me.loadPage();
				});
	};

	/**
	 * 
	 * @returns {Promise}
	 */
	App.prototype.logout = function() {
		return Model.getJson('api/login', null, 'DELETE');
	};

	/**
	 * 
	 * @returns {Promise}
	 */
	App.prototype.currentUser = function() {
		var me = this;

		return Promise.resolve(this._currentUser
				|| new Promise(function(resolve, reject) {
					new User().getResource('api/login', null).then(
							function(user) {
								me._currentUser = user;
								resolve(me._currentUser);
							}, function(err) {
								reject(err);
							});
				}));
	};

	/**
	 * 
	 * @returns {Promise}
	 */
	App.prototype.inkCartridges = function() {
		return new InkCartridge().getResource('api/ink_cartridges', null);
	};

	/**
	 * 
	 * @param id
	 * @returns {Promise}
	 */
	App.prototype.inkCartridge = function(id) {
		return new InkCartridge().getResource('api/ink_cartridge', {
			id : id
		});
	};

	/**
	 * 
	 * @param year
	 * @param month
	 * @param userName
	 * @returns {Promise}
	 */
	App.prototype.logs = function(year, month, userName) {
		return Promise.all([ new ReplaceLog().getResource('api/raplace_log', {
			year : year,
			month : month,
			userName : userName
		}), new TakeOutLog.getResource('api/take_out_log', {
			year : year,
			month : month,
			userName : userName
		}) ]);
	};

	/**
	 * 
	 * @param inkCartridges
	 * @returns {Promise}
	 */
	App.prototype.replace = function(inkCartridges) {
		return new InkCartridge().getResource('api/replace', {
			id : inkCartridges.map(function(inkCartridge) {
				return inkCartridge.id;
			}),
			number : inkCartridges.map(function(inkCartridge) {
				return inkCartridge.replaceNumber;
			}),
		}, 'POST');
	};

	/**
	 * 
	 * @param studentId
	 * @param studentName
	 * @param inkCartridges
	 * @returns {Promise}
	 */
	App.prototype.takeOut = function(studentId, studentName, inkCartridges) {
		return new InkCartridge().getResource('api/take_out', {
			studentId : studentId,
			studentName : studentName,
			id : inkCartridges.map(function(inkCartridge) {
				return inkCartridge.id;
			}),
			number : inkCartridges.map(function(inkCartridge) {
				return inkCartridge.replaceNumber;
			}),
		}, 'POST');
	};

	new App().route('/', function() {
		document.getElementById('main').innerHTML = '<ink-cartridge-stocks />';
	});

	new App().route('/logout', function() {
		var me = this;

		this.logout().then(function() {
			me.loadPage('./');
		});
	});

	new App().route('/ink_cartridges', function() {
		document.getElementById('main').innerHTML = '<ink-cartridges />';
	});

	new App().route('/log', function() {
		document.getElementById('main').innerHTML = '<ink-cartridge-log />';
	});

	new App()
			.route(
					'/replace',
					function(params) {
						document.getElementById('main').innerHTML = '<replace-ink-cartridge />';
					});

	new App()
			.route(
					'/take_out',
					function(params) {
						document.getElementById('main').innerHTML = '<take-out-ink-cartridge />';
					});

	new App().route('/user/:id', function(id) {
		// TODO Not implemented.
		console.log([ '/user/:id', parseInt(id) ]);
	});

	global.App = App;
	global.InkCartridge = InkCartridge;
	global.User = User;
	global.TakeOutLog = TakeOutLog;
	global.ReplaceLog = ReplaceLog;
}(this.self || global));