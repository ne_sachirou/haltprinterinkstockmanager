<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex,nofollow,noarchive" />
<title>${fn:escapeXml(title)}|HAL東京プリンターインク管理システム</title>
<base href="${fn:escapeXml(baseUrl)}/" />
<link rel="home" href="/" />
<link rel="stylesheet" href="resource/style.css" />
<script src="resource/bower_components/platform/platform.js"></script>
<script src="resource/bower_components/bluebird/js/browser/bluebird.js"></script>
<script src="resource/bower_components/uri.js/src/URI.js"></script>
<script src="resource/bower_components/zepto/zepto.js"></script>
<link rel="import" href="resource/bower_components/polymer/polymer.html">
<script src="resource/polyfill.js"></script>
<script src="resource/functil.js"></script>
<script src="resource/model.js"></script>
<script src="resource/app.js"></script>
<link rel="import" href="resource/app.html" />
</head>
<polymer-body> <header class="global-header">
	<div class="global-menu">
		<h1 class="logo">
			<a href="./">HAL東京プリンターインク管理システム</a>
		</h1>
		<nav class="global-menu-login" id="ui-login">
			<global-login-menu />
		</nav>
	</div>
	<nav class="local-menu">
		<div class="menu-item">
			<a href="ink_cartridges">インクカートリッジ設定</a>
		</div>
		<div class="menu-item">
			<a href="logs">補充・持ち出しログ</a>
		</div>
	</nav>
</header>
<article id="main" class="global-main"><jsp:include
		page="/view/partial/index.jsp"></jsp:include></article>
<script>
	new App().init();
</script> </polymer-body>
</html>