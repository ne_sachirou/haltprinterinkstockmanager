package jp.ac.hal.tokyo.c4se.inkstockmanager.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.User;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.SessionRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository;
import lombok.val;

/**
 * Servlet Filter implementation class AccessControllFilter
 */
@WebFilter("/*")
public class AccessControllFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public AccessControllFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		val currentUserName = new SessionRepository(
				(HttpServletRequest) request).getCurrentUserName();
		User currentUser = null;
		if (currentUserName != null) {
			try {
				currentUser = new UserRepository().findByName(currentUserName);
			} catch (DBAccessException e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("currentUser", currentUser);
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
