package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.User;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.SessionRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository;
import lombok.val;

/**
 * Servlet implementation class ApiLoginController
 */
@WebServlet("/api/login")
public class ApiLoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiLoginController() {
		super();
	}

	/**
	 * Get current User.
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		val currentUser = trait.getCurrentUser(request);
		if (currentUser == null) {
			trait.sendApiOk(response);
			return;
		}
		currentUser.setPassword(null);
		trait.sendApiOk(response, currentUser);
	}

	/**
	 * Login.
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String rawPassword = request.getParameter("password");
		@val
		User user;
		try {
			user = new UserRepository().findByName(name);
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		if (user == null || !user.getPassword().equals(rawPassword)) {
			trait.sendApiError(response, 403, "ID or password is wrong.");
			return;
		}
		new SessionRepository(request).setCurrentUserName(user.getName());
		user.setPassword(null);
		trait.sendApiOk(response, user);
	}

	/**
	 * Logout.
	 * 
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		new SessionRepository(request).setCurrentUserName(null);
		trait.sendApiOk(response);
	}

}
