package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository;
import lombok.val;

/**
 * Servlet implementation class ApiInkCartridges
 */
@WebServlet("/api/ink_cartridges")
public class ApiInkCartridgesController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiInkCartridgesController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (trait.getCurrentUser(request) == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		@val
		List<InkCartridge> models;
		try {
			models = new InkCartridgeRepository().all();
		} catch (Exception e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		trait.sendApiOk(response, models);
	}

}
