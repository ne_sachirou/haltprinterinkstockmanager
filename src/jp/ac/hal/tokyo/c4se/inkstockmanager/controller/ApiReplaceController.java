package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.validator.ReplaceValidator;
import lombok.val;

/**
 * Servlet implementation class ReplaceController
 */
@WebServlet("/api/replace")
public class ApiReplaceController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiReplaceController() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		val currentUser = trait.getCurrentUser(request);
		if (currentUser == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		ReplaceValidator validator = new ReplaceValidator();
		try {
			validator.validate(request);
		} catch (ValidationException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		List<ReplaceLog> logs = new ArrayList<>();
		for (val replacement : validator.getReplacements()) {
			ReplaceLog log = new ReplaceLog();
			log.setInkCartridgeId(replacement.getInkCartridgeId());
			log.setInkCartridgeNumber(replacement.getInkCartridgeNumber());
			log.setUserId(currentUser.getId());
			log.setTime(new Date());
			logs.add(log);
		}
		try {
			val repo = new ReplaceLogRepository();
			for (val log : logs) {
				repo.insert(log);
			}
			trait.sendApiOk(response, new InkCartridgeRepository().all());
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
	}

}
