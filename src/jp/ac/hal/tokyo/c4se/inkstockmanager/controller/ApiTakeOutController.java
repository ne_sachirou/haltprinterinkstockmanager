package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.validator.TakeOutValidator;
import lombok.val;

/**
 * Servlet implementation class TakeOutController
 */
@WebServlet("/api/take_out")
public class ApiTakeOutController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiTakeOutController() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		val currentUser = trait.getCurrentUser(request);
		if (currentUser == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		TakeOutValidator validator = new TakeOutValidator();
		try {
			validator.validate(request);
		} catch (ValidationException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		List<TakeOutLog> logs = new ArrayList<>();
		for (val takingOut : validator.getTakingOuts()) {
			TakeOutLog log = new TakeOutLog();
			log.setInkCartridgeId(takingOut.getInkCartridgeId());
			log.setInkCartridgeNumber(takingOut.getInkCartridgeNumber());
			log.setStudentId(validator.getStudentId());
			log.setStudentName(validator.getStudentName());
			log.setUserId(currentUser.getId());
			log.setTime(new Date());
			logs.add(log);
		}
		try {
			val repo = new TakeOutLogRepository();
			for (val log : logs) {
				repo.insert(log);
			}
			trait.sendApiOk(response, new InkCartridgeRepository().all());
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
	}

}
