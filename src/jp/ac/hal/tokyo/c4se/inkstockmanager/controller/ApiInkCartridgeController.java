package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository;
import lombok.val;

/**
 * Servlet implementation class ApiInkCartridge
 */
@WebServlet("/api/ink_cartridge")
public class ApiInkCartridgeController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiInkCartridgeController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (trait.getCurrentUser(request) == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		@val
		InkCartridge inkCartridge;
		try {
			inkCartridge = getModel(request);
		} catch (NumberFormatException | DBAccessException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		trait.sendApiOk(response, inkCartridge);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (trait.getCurrentUser(request) == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		InkCartridge inkCartridge = new InkCartridge();
		try {
			inkCartridge.setName(request.getParameter("name"));
			inkCartridge.setLowerLimitNumber(Integer.parseInt(request
					.getParameter("lowerLimitNumber")));
		} catch (NumberFormatException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		try {
			new InkCartridgeRepository().insert(inkCartridge);
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		trait.sendApiOk(response, inkCartridge);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (trait.getCurrentUser(request) == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		InkCartridge inkCartridge;
		try {
			inkCartridge = getModel(request);
		} catch (NumberFormatException | DBAccessException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		try {
			inkCartridge.setName(request.getParameter("name"));
			inkCartridge.setLowerLimitNumber(Integer.parseInt(request
					.getParameter("lowerLimitNumber")));
		} catch (NumberFormatException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		try {
			inkCartridge = new InkCartridgeRepository().update(inkCartridge);
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		trait.sendApiOk(response, inkCartridge);
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (trait.getCurrentUser(request) == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		InkCartridge inkCartridge;
		try {
			inkCartridge = getModel(request);
		} catch (NumberFormatException | DBAccessException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		try {
			new InkCartridgeRepository().delete(inkCartridge);
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		trait.sendApiOk(response);
	}

	private InkCartridge getModel(HttpServletRequest request)
			throws NumberFormatException, DBAccessException {
		val id = Integer.parseInt(request.getParameter("id"));
		return new InkCartridgeRepository().find(id);
	}

}
