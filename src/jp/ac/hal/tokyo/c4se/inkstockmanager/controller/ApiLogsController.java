package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository;
import jp.ac.hal.tokyo.c4se.inkstockmanager.validator.LogsValidator;
import lombok.val;

/**
 * Servlet implementation class ApiLogController
 */
@WebServlet("/api/logs")
public class ApiLogsController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApiLogsController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		val currentUser = trait.getCurrentUser(request);
		if (currentUser == null) {
			trait.sendApiError(response, 403, "Please login.");
			return;
		}
		LogsValidator validator = new LogsValidator();
		try {
			validator.validate(request);
		} catch (ValidationException e) {
			trait.sendApiError(response, 403, e);
			return;
		}
		val year = validator.getYear();
		val month = validator.getMonth();
		val userName = validator.getUserName();
		@val
		List<ReplaceLog> replaceLogs;
		@val
		List<TakeOutLog> takeOutLogs;
		try {
			if (userName == null) {
				replaceLogs = new ReplaceLogRepository()
						.allByMonth(year, month);
				takeOutLogs = new TakeOutLogRepository()
						.allByMonth(year, month);
			} else {
				replaceLogs = new ReplaceLogRepository().allByMonthAndUserName(
						year, month, userName);
				takeOutLogs = new TakeOutLogRepository().allByMonthAndUserName(
						year, month, userName);
			}
		} catch (DBAccessException e) {
			trait.sendApiError(response, 502, e);
			return;
		}
		Map<String, Object> result = new HashMap<>();
		result.put("replaceLogs", replaceLogs);
		result.put("takeOutLogs", takeOutLogs);
		trait.sendApiOk(response, result);
	}

}
