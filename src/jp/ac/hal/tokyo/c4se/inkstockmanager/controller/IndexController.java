package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class IndexController
 */
@WebServlet({ "/" })
public class IndexController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final ControllerTrait trait = new ControllerTrait();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		trait.setCommonParameters(request);
		request.getRequestDispatcher("/view/layout/layout.jsp").forward(
				request, response);
	}

}
