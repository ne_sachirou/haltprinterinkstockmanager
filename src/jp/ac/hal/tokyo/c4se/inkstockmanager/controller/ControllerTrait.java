/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.tokyo.c4se.inkstockmanager.model.User;
import net.arnx.jsonic.JSON;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
class ControllerTrait {

	/**
	 * 
	 * @param response
	 * @param status
	 * @param message
	 * @throws IOException
	 */
	public void sendApiError(HttpServletResponse response, int status,
			String message) throws IOException {
		response.setStatus(status);
		Map<String, String> map = new HashMap<>();
		map.put("error", message);
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getOutputStream().write(
				new JSON().format(map).getBytes("UTF-8"));
	}

	/**
	 * 
	 * @param response
	 * @param status
	 * @param e
	 * @throws IOException
	 */
	public void sendApiError(HttpServletResponse response, int status,
			Throwable e) throws IOException {
		if (status >= 500) {
			e.printStackTrace();
		}
		sendApiError(response, status, e.getMessage());
	}

	/**
	 * 
	 * @param response
	 * @param model
	 * @throws IOException
	 */
	public void sendApiOk(HttpServletResponse response, Object model)
			throws IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getOutputStream().write(
				new JSON().format(model).getBytes("UTF-8"));
	}

	/**
	 * 
	 * @param response
	 * @throws IOException
	 */
	public void sendApiOk(HttpServletResponse response) throws IOException {
		Map<String, String> map = new HashMap<>();
		map.put("ok", "ok");
		sendApiOk(response, map);
	}

	/**
	 * 
	 * @param request
	 */
	public void setCommonParameters(HttpServletRequest request) {
		request.setAttribute("baseUrl", request.getSession()
				.getServletContext().getContextPath());
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public User getCurrentUser(HttpServletRequest request) {
		return (User) request.getAttribute("currentUser");
	}

}
