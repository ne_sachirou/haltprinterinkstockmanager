/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;

/**
 * インクカートリッジの種類
 * 
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
@Entity
@Table(name = "ink_cartridge")
@Data
public class InkCartridge implements Serializable, EntityModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name; // 種類名

	@Column(name = "lower_limit_number")
	private int lowerLimitNumber = 10; // 発注点

	@Column(name = "number")
	private int number; // 残数

	@Column(name = "deleted_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedAt = null;

	@Override
	public void validate() throws ValidationException {
		if (name == null || name.isEmpty()) {
			throw new ValidationException("InkCartridge.name is can't be null.");
		}
		if (lowerLimitNumber < 0) {
			throw new ValidationException(
					"InkCartridge.lowerLimitNumber can't be negative.");
		}
		if (number < 0) {
			throw new ValidationException(
					"InkCartridge.number can't be negative.");
		}
	}

}
