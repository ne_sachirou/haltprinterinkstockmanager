package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-04-03T19:45:11.688+0900")
@StaticMetamodel(ReplaceLog.class)
public class ReplaceLog_ {
	public static volatile SingularAttribute<ReplaceLog, Integer> id;
	public static volatile SingularAttribute<ReplaceLog, Date> time;
	public static volatile SingularAttribute<ReplaceLog, Integer> userId;
	public static volatile SingularAttribute<ReplaceLog, User> user;
	public static volatile SingularAttribute<ReplaceLog, Integer> inkCartridgeId;
	public static volatile SingularAttribute<ReplaceLog, InkCartridge> inkCartridge;
	public static volatile SingularAttribute<ReplaceLog, Integer> inkCartridgeNumber;
}
