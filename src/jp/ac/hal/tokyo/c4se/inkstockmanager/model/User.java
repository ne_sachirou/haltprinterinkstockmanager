/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;

/**
 * ユーザー
 * 
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
@Entity
@Table(name = "user")
@Data
public class User implements Serializable, EntityModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "password")
	private String password;

	@Column(name = "deleted_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedAt = null;

	@Override
	public void validate() throws ValidationException {
	}

}
