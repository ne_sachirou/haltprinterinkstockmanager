package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-04-03T19:45:11.704+0900")
@StaticMetamodel(TakeOutLog.class)
public class TakeOutLog_ {
	public static volatile SingularAttribute<TakeOutLog, Integer> id;
	public static volatile SingularAttribute<TakeOutLog, Date> time;
	public static volatile SingularAttribute<TakeOutLog, Integer> userId;
	public static volatile SingularAttribute<TakeOutLog, User> user;
	public static volatile SingularAttribute<TakeOutLog, String> studentId;
	public static volatile SingularAttribute<TakeOutLog, String> studentName;
	public static volatile SingularAttribute<TakeOutLog, Integer> inkCartridgeId;
	public static volatile SingularAttribute<TakeOutLog, InkCartridge> inkCartridge;
	public static volatile SingularAttribute<TakeOutLog, Integer> inkCartridgeNumber;
}
