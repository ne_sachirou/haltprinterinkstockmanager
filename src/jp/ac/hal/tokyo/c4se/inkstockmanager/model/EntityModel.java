/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public interface EntityModel {

	/**
	 * 
	 * @throws ValidationException
	 */
	public void validate() throws ValidationException;

}
