package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-04-03T19:45:11.468+0900")
@StaticMetamodel(InkCartridge.class)
public class InkCartridge_ {
	public static volatile SingularAttribute<InkCartridge, Integer> id;
	public static volatile SingularAttribute<InkCartridge, String> name;
	public static volatile SingularAttribute<InkCartridge, Integer> lowerLimitNumber;
	public static volatile SingularAttribute<InkCartridge, Integer> number;
	public static volatile SingularAttribute<InkCartridge, Date> deletedAt;
}
