/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;

/**
 * 持ち出しログ
 * 
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
@Entity
@Table(name = "take_out_log")
@Data
public class TakeOutLog implements Serializable, EntityModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date time;

	@Column(name = "user_id")
	private int userId;

	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;

	@Column(name = "student_id")
	private String studentId; // 学籍番号

	@Column(name = "student_name")
	private String studentName;

	@Column(name = "ink_cartridge_id")
	private int inkCartridgeId;

	@ManyToOne
	@JoinColumn(name = "inkCartridgeId")
	private InkCartridge inkCartridge;

	@Column(name = "ink_cartridge_number")
	private int inkCartridgeNumber;

	@Override
	public void validate() throws ValidationException {
	}

}
