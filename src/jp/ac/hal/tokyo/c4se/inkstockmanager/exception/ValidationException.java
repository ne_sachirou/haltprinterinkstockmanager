/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.exception;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValidationException() {
		super();
	}

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(Exception e) {
		super(e);
	}

}
