/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.exception;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class DBAccessException extends Exception {

	private static final long serialVersionUID = 1L;

	public DBAccessException() {
		super();
	}

	public DBAccessException(String message) {
		super(message);
	}

	public DBAccessException(Exception e) {
		super(e);
	}

}
