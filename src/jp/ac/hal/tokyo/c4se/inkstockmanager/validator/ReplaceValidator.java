/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.validator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;
import lombok.val;

/**
 * 補充
 * 
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
@Data
public class ReplaceValidator implements Validator {

	@Data
	public class Replacement {
		private int inkCartridgeId;
		private int inkCartridgeNumber;
	}

	private List<Replacement> replacements;

	@Override
	public void validate(HttpServletRequest request) throws ValidationException {
		replacements = new ArrayList<>();
		val inkCartridgeIdStrs = request.getParameterValues("id");
		val inkCartridgeNumberStrs = request.getParameterValues("number");
		if (inkCartridgeIdStrs == null || inkCartridgeIdStrs.length == 0) {
			throw new ValidationException("There is no InkCartridges.");
		}
		if (inkCartridgeNumberStrs == null
				|| inkCartridgeNumberStrs.length == 0) {
			throw new ValidationException("There is no InkCartridge numbers.");
		}
		for (int i = 0; i < Math.min(inkCartridgeIdStrs.length,
				inkCartridgeNumberStrs.length); ++i) {
			val replacement = new Replacement();
			try {
				replacement.setInkCartridgeId(Integer
						.parseInt(inkCartridgeIdStrs[i]));
				replacement.setInkCartridgeNumber(Integer
						.parseInt(inkCartridgeNumberStrs[i]));
			} catch (NumberFormatException e) {
				throw new ValidationException(e);
			}
			replacements.add(replacement);
		}
		for (val replacement : replacements) {
			val inkCartridgeNumber = replacement.getInkCartridgeNumber();
			if (inkCartridgeNumber < 0) {
				throw new ValidationException(String.format(
						"There is no InkCartridge.number = %d.",
						inkCartridgeNumber));
			}
		}
	}
}
