/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.validator;

import javax.servlet.http.HttpServletRequest;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public interface Validator {

	/**
	 * 
	 * @param request
	 * @throws ValidationException
	 */
	public void validate(HttpServletRequest request) throws ValidationException;

}
