/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.validator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;
import lombok.val;

/**
 * 持ち出し
 * 
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
@Data
public class TakeOutValidator implements Validator {

	@Data
	public class TakingOut {
		private int inkCartridgeId;
		private int inkCartridgeNumber;
	}

	private String studentId;
	private String studentName;
	private List<TakingOut> takingOuts;

	@Override
	public void validate(HttpServletRequest request) throws ValidationException {
		studentId = request.getParameter("studentId");
		studentName = request.getParameter("studentName");
		takingOuts = new ArrayList<>();
		val inkCartridgeIdStrs = request.getParameterValues("id");
		val inkCartridgeNumberStrs = request.getParameterValues("number");
		for (int i = 0; i < Math.min(inkCartridgeIdStrs.length,
				inkCartridgeNumberStrs.length); ++i) {
			val takingOut = new TakingOut();
			try {
				takingOut.setInkCartridgeId(Integer
						.parseInt(inkCartridgeIdStrs[i]));
				takingOut.setInkCartridgeNumber(Integer
						.parseInt(inkCartridgeNumberStrs[i]));
			} catch (NumberFormatException e) {
				throw new ValidationException(e);
			}
			takingOuts.add(takingOut);
		}
		if (studentId == null || studentId.isEmpty()) {
			throw new ValidationException("studentId is required.");
		}
		if (studentName == null || studentName.isEmpty()) {
			throw new ValidationException("studentName is required.");
		}
		for (val takingOut : takingOuts) {
			val inkCartridgeNumber = takingOut.getInkCartridgeNumber();
			if (inkCartridgeNumber < 0) {
				throw new ValidationException(String.format(
						"There is no InkCartridge.number = %d.",
						inkCartridgeNumber));
			}
		}
	}

}
