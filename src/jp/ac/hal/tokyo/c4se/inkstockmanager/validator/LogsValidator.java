/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.validator;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.ValidationException;
import lombok.Data;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 * 
 */
@Data
public class LogsValidator implements Validator {

	private int year;
	private int month;
	private String userName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jp.ac.hal.tokyo.c4se.inkstockmanager.validator.Validator#validate(javax
	 * .servlet.http.HttpServletRequest)
	 */
	@Override
	public void validate(HttpServletRequest request) throws ValidationException {
		try {
			year = Integer.parseInt(request.getParameter("year"));
			month = Integer.parseInt(request.getParameter("month"));
			userName = request.getParameter("userName");
		} catch (NumberFormatException e) {
			throw new ValidationException(e);
		}
		if (year < 1970 || Calendar.getInstance().get(Calendar.YEAR) < year) {
			throw new ValidationException(String.format(
					"year = %d is invalid.", year));
		}
		if (month < 1 || 12 < month) {
			throw new ValidationException(String.format(
					"month = %02d is invalid.", month));
		}
		if (userName.isEmpty()) {
			userName = null;
		}
	}

}
