/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import lombok.val;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
class RepositoryTrait {

	/**
	 * 
	 * @return
	 */
	public EntityManager getEntityManager() throws DBAccessException {
		try {
			val factory = Persistence
					.createEntityManagerFactory("HALtPrinterInkStockManager");
			return factory.createEntityManager();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @throws DBAccessException
	 */
	public void setUpTest() throws DBAccessException {
		EntityTransaction transaction = null;
		try {
			val manager = getEntityManager();
			transaction = manager.getTransaction();
			// http://stackoverflow.com/questions/13011892/how-to-locate-the-path-of-the-current-project-in-java-eclipse
			val dirName = System.getProperty("user.dir").replace("\\", "/")
					+ "/sql";
			transaction.begin();
			for (val table : new String[][] {
					{ "ink_cartridge",
							"id, name, lower_limit_number, number, deleted_at" },
					{ "user", "id, name, password, deleted_at" },
					{ "replace_log",
							"id, time, user_id, ink_cartridge_id, ink_cartridge_number" },
					{
							"take_out_log",
							"id, time, user_id, student_id, student_name, ink_cartridge_id, ink_cartridge_number" } }) {
				val tableName = table[0];
				val columnNames = table[1];
				val query = manager
						.createNativeQuery(String
								.format("load data local infile '%s/%s.csv' into table %s fields terminated by ',' optionally enclosed by '\"' ignore 1 lines (%s)",
										dirName, tableName, tableName,
										columnNames));
				query.executeUpdate();
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @throws DBAccessException
	 */
	public void tearDownTest() throws DBAccessException {
		EntityTransaction transaction = null;
		try {
			val manager = getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			for (val table : new String[][] { { "TakeOutLog", "take_out_log" },
					{ "ReplaceLog", "replace_log" }, { "User", "user" },
					{ "InkCartridge", "ink_cartridge" } }) {
				val className = table[0];
				val tableName = table[1];
				Query query = manager.createQuery("delete from " + className);
				query.executeUpdate();
				query = manager.createNativeQuery("alter table " + tableName
						+ " auto_increment = 1");
				query.executeUpdate();
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

}
