/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import static org.junit.Assert.*;
import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import lombok.val;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class RepositoryTraitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		val trait = new RepositoryTrait();
		trait.tearDownTest();
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.RepositoryTrait#getEntityManager()}
	 * .
	 */
	@Test
	public final void testGetEntityManager() {
		val trait = new RepositoryTrait();
		try {
			assertNotNull(trait.getEntityManager());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.RepositoryTrait#setUpTest()}
	 * .
	 */
	@Test
	public final void testSetUpTest() {
		val trait = new RepositoryTrait();
		try {
			trait.setUpTest();
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.RepositoryTrait#tearDownTest()}
	 * .
	 */
	@Test
	public final void testTearDownTest() {
		val trait = new RepositoryTrait();
		try {
			trait.tearDownTest();
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
