/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityTransaction;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog;
import lombok.val;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class ReplaceLogRepository implements Repositiry<ReplaceLog> {

	private final RepositoryTrait trait = new RepositoryTrait();

	/**
	 * 
	 * @param id
	 * @return
	 * @throws DBAccessException
	 */
	public ReplaceLog find(int id) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			return manager.find(ReplaceLog.class, id);
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public List<ReplaceLog> all() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager.createQuery("select rl from ReplaceLog rl",
					ReplaceLog.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @param year
	 * @param month
	 *            1-12
	 * @return
	 * @throws DBAccessException
	 */
	public List<ReplaceLog> allByMonth(int year, int month)
			throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery(
							"select rl from ReplaceLog rl where rl.time between :begin_date and :end_date",
							ReplaceLog.class);
			Calendar cal = Calendar.getInstance();
			cal.set(year, month - 1, 0);
			query.setParameter("begin_date", cal.getTime());
			cal.set(year, month, 0);
			query.setParameter("end_date", cal.getTime());
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @param year
	 * @param month
	 *            1-12
	 * @param userName
	 * @return
	 * @throws DBAccessException
	 */
	public List<ReplaceLog> allByMonthAndUserName(int year, int month,
			String userName) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery(
							"select rl from ReplaceLog rl join User u on rl.userId = u.id where rl.time between :begin_date and :end_date and u.name = :user_name",
							ReplaceLog.class);
			Calendar cal = Calendar.getInstance();
			cal.set(year, month - 1, 0);
			query.setParameter("begin_date", cal.getTime());
			cal.set(year, month, 0);
			query.setParameter("end_date", cal.getTime());
			query.setParameter("user_name", userName);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public long count() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery("select count(rl) From ReplaceLog rl");
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public void insert(ReplaceLog model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			InkCartridge inkCartridge = manager.find(InkCartridge.class,
					model.getInkCartridgeId());
			inkCartridge.setNumber(inkCartridge.getNumber()
					+ model.getInkCartridgeNumber());
			inkCartridge.validate();
			manager.merge(inkCartridge);
			model.validate();
			manager.persist(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

	@Override
	public ReplaceLog update(ReplaceLog model) throws DBAccessException {
		throw new DBAccessException("ReplaceLog can not be updated.");
	}

	@Override
	public void delete(ReplaceLog model) throws DBAccessException {
		throw new DBAccessException("ReplaceLog can not be deleted.");
	}

}
