/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.List;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.EntityModel;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public interface Repositiry<M extends EntityModel> {

	/**
	 * 
	 * @return
	 * @throws DBAccessException
	 */
	public List<M> all() throws DBAccessException;

	/**
	 * 
	 * @return
	 * @throws DBAccessException
	 */
	public long count() throws DBAccessException;

	/**
	 * 
	 * @param model
	 * @throws DBAccessException
	 */
	public void insert(M model) throws DBAccessException;

	/**
	 * 
	 * @param model
	 * @return
	 * @throws DBAccessException
	 */
	public M update(M model) throws DBAccessException;

	/**
	 * 
	 * @param model
	 * @throws DBAccessException
	 */
	public void delete(M model) throws DBAccessException;

}
