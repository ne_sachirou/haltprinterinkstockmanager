/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityTransaction;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog;
import lombok.val;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class TakeOutLogRepository implements Repositiry<TakeOutLog> {

	private final RepositoryTrait trait = new RepositoryTrait();

	/**
	 * 
	 * @param id
	 * @return
	 * @throws DBAccessException
	 */
	public TakeOutLog find(int id) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			return manager.find(TakeOutLog.class, id);
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public List<TakeOutLog> all() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager.createQuery("select tol from TakeOutLog tol",
					TakeOutLog.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @param year
	 * @param month
	 *            1-12
	 * @return
	 * @throws DBAccessException
	 */
	public List<TakeOutLog> allByMonth(int year, int month)
			throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery(
							"select tol from TakeOutLog tol where tol.time between :begin_date and :end_date",
							TakeOutLog.class);
			Calendar cal = Calendar.getInstance();
			cal.set(year, month - 1, 0);
			query.setParameter("begin_date", cal.getTime());
			cal.set(year, month, 0);
			query.setParameter("end_date", cal.getTime());
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @param year
	 * @param month
	 *            1-12
	 * @param userName
	 * @return
	 * @throws DBAccessException
	 */
	public List<TakeOutLog> allByMonthAndUserName(int year, int month,
			String userName) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery(
							"select tol from TakeOutLog tol join User u on tol.userId = u.id where tol.time between :begin_date and :end_date and u.name = :user_name",
							TakeOutLog.class);
			Calendar cal = Calendar.getInstance();
			cal.set(year, month - 1, 0);
			query.setParameter("begin_date", cal.getTime());
			cal.set(year, month, 0);
			query.setParameter("end_date", cal.getTime());
			query.setParameter("user_name", userName);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public long count() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery("select count(tol) From TakeOutLog tol");
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public void insert(TakeOutLog model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			InkCartridge inkCartridge = manager.find(InkCartridge.class,
					model.getInkCartridgeId());
			inkCartridge.setNumber(inkCartridge.getNumber()
					- model.getInkCartridgeNumber());
			inkCartridge.validate();
			manager.merge(inkCartridge);
			model.validate();
			manager.persist(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

	@Override
	public TakeOutLog update(TakeOutLog model) throws DBAccessException {
		throw new DBAccessException("TakeOutLog can not be updated.");
	}

	@Override
	public void delete(TakeOutLog model) throws DBAccessException {
		throw new DBAccessException("TakeOutLog can not be deleted.");
	}

}
