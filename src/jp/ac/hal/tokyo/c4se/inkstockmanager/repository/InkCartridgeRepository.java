/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityTransaction;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import lombok.val;

/**
 * @author ne_Sachieou <utakata.c4se@gmail.com>
 */
public class InkCartridgeRepository implements Repositiry<InkCartridge> {

	private final RepositoryTrait trait = new RepositoryTrait();

	/**
	 * 
	 * @param id
	 * @return
	 * @throws DBAccessException
	 */
	public InkCartridge find(int id) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			return manager.find(InkCartridge.class, id);
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public List<InkCartridge> all() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery(
							"select ic from InkCartridge ic where ic.deletedAt is null",
							InkCartridge.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public long count() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery("select count(ic) From InkCartridge ic where ic.deletedAt is null");
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public void insert(InkCartridge model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			manager.persist(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

	@Override
	public InkCartridge update(InkCartridge model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			model = manager.merge(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
		return model;
	}

	@Override
	public void delete(InkCartridge model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			model.setDeletedAt(new Date());
			manager.merge(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

}
