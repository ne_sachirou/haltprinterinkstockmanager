/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityTransaction;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.User;
import lombok.val;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class UserRepository implements Repositiry<User> {

	private final RepositoryTrait trait = new RepositoryTrait();

	/**
	 * 
	 * @param id
	 * @return
	 * @throws DBAccessException
	 */
	public User find(int id) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			return manager.find(User.class, id);
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	/**
	 * 
	 * @param name
	 * @return
	 * @throws DBAccessException
	 */
	public User findByName(String name) throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager.createQuery(
					"select u from User u where u.name = :name", User.class);
			query.setParameter("name", name);
			val models = query.getResultList();
			if (models.size() == 0) {
				return null;
			}
			return models.get(0);
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public List<User> all() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager.createQuery(
					"select u from User u where u.deletedAt is null",
					User.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public long count() throws DBAccessException {
		val manager = trait.getEntityManager();
		try {
			val query = manager
					.createQuery("select count(u) From User u where u.deletedAt is null");
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			throw new DBAccessException(e);
		}
	}

	@Override
	public void insert(User model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			manager.persist(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

	@Override
	public User update(User model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			model = manager.merge(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
		return model;
	}

	@Override
	public void delete(User model) throws DBAccessException {
		val manager = trait.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			model.setDeletedAt(new Date());
			manager.merge(model);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				try {
					transaction.rollback();
				} catch (Exception e1) {
					throw new DBAccessException(e1);
				}
			}
			throw new DBAccessException(e);
		}
	}

}
