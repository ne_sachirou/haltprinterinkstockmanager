/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import static org.junit.Assert.*;

import java.util.List;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.User;
import lombok.val;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class UserRepositoryTest {

	RepositoryTrait trait;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		trait = new RepositoryTrait();
		trait.setUpTest();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		trait.tearDownTest();
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#find(int)}
	 * .
	 */
	@Test
	public final void testFind() {
		val repo = new UserRepository();
		try {
			User actual = repo.find(1);
			assertEquals(1, actual.getId());
			assertEquals("ths10000", actual.getName());
			assertNull(actual.getDeletedAt());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public final void testFindByName() {
		val repo = new UserRepository();
		try {
			User actual = repo.findByName("ths10000");
			assertEquals(1, actual.getId());
			assertEquals("ths10000", actual.getName());
			assertNull(actual.getDeletedAt());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#all()}
	 * .
	 */
	@Test
	public final void testAll() {
		val repo = new UserRepository();
		try {
			List<User> actual = repo.all();
			assertEquals(9, actual.size());
			assertEquals(1, actual.get(0).getId());
			assertEquals("ths10000", actual.get(0).getName());
			assertNull(actual.get(0).getDeletedAt());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#count()}
	 * .
	 */
	@Test
	public final void testCount() {
		val repo = new UserRepository();
		try {
			assertEquals(9, repo.count());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#insert(jp.ac.hal.tokyo.c4se.inkstockmanager.model.User)}
	 * .
	 */
	@Test
	public final void testInsert() {
		val repo = new UserRepository();
		User expected = new User();
		expected.setName("thsTSET");
		expected.setPassword("passwordTEST");
		try {
			repo.insert(expected);
			assertEquals(11, expected.getId());
			User actual = repo.find(11);
			assertEquals(expected, actual);
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#update(jp.ac.hal.tokyo.c4se.inkstockmanager.model.User)}
	 * .
	 */
	@Test
	public final void testUpdate() {
		val repo = new UserRepository();
		try {
			User expected = repo.find(1);
			expected.setName("thsTEST");
			User actual = repo.update(expected);
			assertEquals(expected, actual);
			actual = repo.find(1);
			assertEquals(expected, actual);
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.UserRepository#delete(jp.ac.hal.tokyo.c4se.inkstockmanager.model.User)}
	 * .
	 */
	@Test
	public final void testDelete() {
		val repo = new UserRepository();
		try {
			User model = repo.find(1);
			assertNull(model.getDeletedAt());
			repo.delete(model);
			assertNotNull(model.getDeletedAt());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
