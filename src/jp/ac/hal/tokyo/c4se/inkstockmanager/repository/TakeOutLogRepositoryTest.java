/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog;
import lombok.val;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 * 
 */
public class TakeOutLogRepositoryTest {

	RepositoryTrait trait;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		trait = new RepositoryTrait();
		trait.setUpTest();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		trait.tearDownTest();
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#find(int)}
	 * .
	 */
	@Test
	public final void testFind() {
		val repo = new TakeOutLogRepository();
		try {
			TakeOutLog actual = repo.find(1);
			assertEquals(getExpectedModel(), actual);
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#all()}
	 * .
	 */
	@Test
	public final void testAll() {
		val repo = new TakeOutLogRepository();
		try {
			List<TakeOutLog> actual = repo.all();
			assertEquals(16, actual.size());
			assertEquals(getExpectedModel(), actual.get(0));
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public final void testAllByMonth() {
		val repo = new TakeOutLogRepository();
		try {
			List<TakeOutLog> actual = repo.allByMonth(2014, 2);
			assertEquals(4, actual.size());
			val cal = Calendar.getInstance();
			cal.setTime(actual.get(0).getTime());
			assertEquals(2014, cal.get(Calendar.YEAR));
			assertEquals(2, cal.get(Calendar.MONTH) + 1);
		} catch (DBAccessException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public final void testAllByMonthAndUserName() {
		val repo = new TakeOutLogRepository();
		try {
			List<TakeOutLog> actual = repo.allByMonthAndUserName(2014, 2,
					"ths10004");
			assertEquals(2, actual.size());
			val cal = Calendar.getInstance();
			cal.setTime(actual.get(0).getTime());
			assertEquals(2014, cal.get(Calendar.YEAR));
			assertEquals(2, cal.get(Calendar.MONTH) + 1);
			assertEquals(5, actual.get(0).getUserId());
		} catch (DBAccessException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#count()}
	 * .
	 */
	@Test
	public final void testCount() {
		val repo = new TakeOutLogRepository();
		try {
			assertEquals(16, repo.count());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#insert(jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog)}
	 * .
	 */
	@Test
	public final void testInsert() {
		val repo = new TakeOutLogRepository();
		try {
			TakeOutLog expected = getExpectedModel();
			val beforeNumber = new InkCartridgeRepository().find(
					expected.getInkCartridgeId()).getNumber();
			repo.insert(expected);
			TakeOutLog actual = repo.find(expected.getId());
			assertEquals(expected, actual);
			val afterNumber = new InkCartridgeRepository().find(
					expected.getInkCartridgeId()).getNumber();
			assertEquals(expected.getInkCartridgeNumber(), beforeNumber
					- afterNumber);
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#update(jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog)}
	 * .
	 */
	@Test
	public final void testUpdate() {
		val repo = new TakeOutLogRepository();
		try {
			repo.update(new TakeOutLog());
			fail("This must cause an error.");
		} catch (DBAccessException e) {
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.TakeOutLogRepository#delete(jp.ac.hal.tokyo.c4se.inkstockmanager.model.TakeOutLog)}
	 * .
	 */
	@Test
	public final void testDelete() {
		val repo = new TakeOutLogRepository();
		try {
			repo.delete(new TakeOutLog());
			fail("This must cause an error.");
		} catch (DBAccessException e) {
		}
	}

	private TakeOutLog getExpectedModel() throws ParseException {
		TakeOutLog expected = new TakeOutLog();
		expected.setId(1);
		expected.setTime(new SimpleDateFormat("yyyy-MM-dd kk:mm")
				.parse("2014-01-01 14:00:00"));
		expected.setUserId(5);
		// expected.setUser(new UserRepository().find(5));
		expected.setStudentId("ths12001");
		expected.setStudentName("一太郎");
		expected.setInkCartridgeId(1);
		// expected.setInkCartridge(new InkCartridgeRepository().find(1));
		expected.setInkCartridgeNumber(1);
		return expected;
	}

}
