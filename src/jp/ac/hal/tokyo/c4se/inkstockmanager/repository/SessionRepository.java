/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class SessionRepository {

	private HttpSession session;

	/**
	 * 
	 */
	public SessionRepository() {
	}

	/**
	 * 
	 * @param request
	 */
	public SessionRepository(HttpServletRequest request) {
		session = request.getSession();
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrentUserName() {
		if (session == null) {
			return null;
		}
		return (String) session.getAttribute("currentUserName");
	}

	/**
	 * 
	 * @param userName
	 */
	public void setCurrentUserName(String userName) {
		if (session == null) {
			return;
		}
		if (userName == null) {
			session.removeAttribute("currentUserName");
			return;
		}
		session.setAttribute("currentUserName", userName);
	}

	/**
	 * 
	 * @return
	 */
	public String getCsrfToken() {
		if (session.getAttribute("csrfToken") == null) {
			session.setAttribute("csrfToken", UUID.randomUUID().toString());
		}
		return (String) session.getAttribute("csrfToken");
	}

}
