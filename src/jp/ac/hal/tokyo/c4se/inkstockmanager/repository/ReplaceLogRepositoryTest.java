/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog;
import lombok.val;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class ReplaceLogRepositoryTest {

	RepositoryTrait trait;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		trait = new RepositoryTrait();
		trait.setUpTest();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		trait.tearDownTest();
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#find(int)}
	 * .
	 */
	@Test
	public final void testFind() {
		val repo = new ReplaceLogRepository();
		try {
			ReplaceLog actual = repo.find(1);
			assertEquals(getExpectedModel(), actual);
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#all()}
	 * .
	 */
	@Test
	public final void testAll() {
		val repo = new ReplaceLogRepository();
		try {
			List<ReplaceLog> actual = repo.all();
			assertEquals(15, actual.size());
			assertEquals(getExpectedModel(), actual.get(0));
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public final void testAllByMonth() {
		val repo = new ReplaceLogRepository();
		try {
			List<ReplaceLog> actual = repo.allByMonth(2014, 2);
			assertEquals(3, actual.size());
			val cal = Calendar.getInstance();
			cal.setTime(actual.get(0).getTime());
			assertEquals(2014, cal.get(Calendar.YEAR));
			assertEquals(2, cal.get(Calendar.MONTH) + 1);
		} catch (DBAccessException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public final void testAllByMonthAndUserName() {
		val repo = new ReplaceLogRepository();
		try {
			List<ReplaceLog> actual = repo.allByMonthAndUserName(2014, 2,
					"ths10004");
			assertEquals(2, actual.size());
			val cal = Calendar.getInstance();
			cal.setTime(actual.get(0).getTime());
			assertEquals(2014, cal.get(Calendar.YEAR));
			assertEquals(2, cal.get(Calendar.MONTH) + 1);
			assertEquals(5, actual.get(0).getUserId());
		} catch (DBAccessException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#count()}
	 * .
	 */
	@Test
	public final void testCount() {
		val repo = new ReplaceLogRepository();
		try {
			assertEquals(15, repo.count());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#insert(jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog)}
	 * .
	 */
	@Test
	public final void testInsert() {
		val repo = new ReplaceLogRepository();
		try {
			ReplaceLog expected = getExpectedModel();
			val beforeNumber = new InkCartridgeRepository().find(
					expected.getInkCartridgeId()).getNumber();
			repo.insert(expected);
			ReplaceLog actual = repo.find(expected.getId());
			assertEquals(expected, actual);
			val afterNumber = new InkCartridgeRepository().find(
					expected.getInkCartridgeId()).getNumber();
			assertEquals(expected.getInkCartridgeNumber(), afterNumber
					- beforeNumber);
		} catch (DBAccessException | ParseException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#update(jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog)}
	 * .
	 */
	@Test
	public final void testUpdate() {
		val repo = new ReplaceLogRepository();
		try {
			repo.update(new ReplaceLog());
			fail("This must cause an error.");
		} catch (DBAccessException e) {
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.ReplaceLogRepository#delete(jp.ac.hal.tokyo.c4se.inkstockmanager.model.ReplaceLog)}
	 * .
	 */
	@Test
	public final void testDelete() {
		val repo = new ReplaceLogRepository();
		try {
			repo.delete(new ReplaceLog());
			fail("This must cause an error.");
		} catch (DBAccessException e) {
		}
	}

	private ReplaceLog getExpectedModel() throws ParseException {
		ReplaceLog expected = new ReplaceLog();
		expected.setId(1);
		expected.setTime(new SimpleDateFormat("yyyy-MM-dd kk:mm")
				.parse("2014-01-02 15:00"));
		expected.setUserId(1);
		// expected.setUser(new UserRepository().find(1));
		expected.setInkCartridgeId(7);
		// expected.setInkCartridge(new InkCartridgeRepository().find(7));
		expected.setInkCartridgeNumber(2);
		return expected;
	}

}
