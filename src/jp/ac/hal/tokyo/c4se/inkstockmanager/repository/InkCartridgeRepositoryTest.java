/**
 * 
 */
package jp.ac.hal.tokyo.c4se.inkstockmanager.repository;

import static org.junit.Assert.*;

import java.util.List;

import jp.ac.hal.tokyo.c4se.inkstockmanager.exception.DBAccessException;
import jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge;
import lombok.val;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 */
public class InkCartridgeRepositoryTest {

	RepositoryTrait trait;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		trait = new RepositoryTrait();
		trait.setUpTest();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		trait.tearDownTest();
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#find(int)}
	 * .
	 */
	@Test
	public final void testFind() {
		val repo = new InkCartridgeRepository();
		try {
			InkCartridge actual = repo.find(1);
			assertEquals(getExpectedModel(), actual);
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#all()}
	 * .
	 */
	@Test
	public final void testAll() {
		val repo = new InkCartridgeRepository();
		try {
			List<InkCartridge> actual = repo.all();
			assertEquals(11, actual.size());
			assertEquals(getExpectedModel(), actual.get(0));
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#count()}
	 * .
	 */
	@Test
	public final void testCount() {
		val repo = new InkCartridgeRepository();
		try {
			assertEquals(11, repo.count());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#insert(jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge)}
	 * .
	 */
	@Test
	public final void testInsert() {
		val repo = new InkCartridgeRepository();
		InkCartridge expected = getExpectedModel();
		try {
			repo.insert(expected);
			InkCartridge actual = repo.find(expected.getId());
			assertEquals(expected, actual);
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#update(jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge)}
	 * .
	 */
	@Test
	public final void testUpdate() {
		val repo = new InkCartridgeRepository();
		InkCartridge expected = getExpectedModel();
		try {
			InkCartridge actual = repo.find(1);
			actual.setName("テスト");
			actual = repo.update(actual);
			expected.setName("テスト");
			assertEquals(expected, actual);
			actual = repo.find(1);
			assertEquals(expected, actual);
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link jp.ac.hal.tokyo.c4se.inkstockmanager.repository.InkCartridgeRepository#delete(jp.ac.hal.tokyo.c4se.inkstockmanager.model.InkCartridge)}
	 * .
	 */
	@Test
	public final void testDelete() {
		val repo = new InkCartridgeRepository();
		try {
			InkCartridge model = repo.find(1);
			assertNull(model.getDeletedAt());
			repo.delete(model);
			assertNotNull(model.getDeletedAt());
		} catch (DBAccessException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private InkCartridge getExpectedModel() {
		InkCartridge expected = new InkCartridge();
		expected.setId(1);
		expected.setName("エプソンインクICBK50黒");
		expected.setLowerLimitNumber(5);
		expected.setNumber(7);
		return expected;
	}

}
